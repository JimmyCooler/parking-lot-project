<?php

namespace App\Exceptions;


class NoSpaceAvailableError extends \Exception
{

    public function __toString()
    {
        __toString("You cannot add more 'active' tickets since the parking lot is fully occupied.");
    }

}
