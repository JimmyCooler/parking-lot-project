<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parking;
use App\Ticket;
use App\User;
use App\Exceptions\InvalidInputException;
use App\Events\ChangeParkingStatus;

class ParkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return Parking::all();
    }

    public function generateTicket(Parking $parking, Request $request)
    {
      $has_parking = $parking->isParkingAvailable();
      $return_arr = ['has_parking' => $has_parking];
      $user = false;
      $email = $request->input('email');
      if($email){
        $user = User::getUserByEmail($email);
      }

      if($has_parking){
        $return_arr['ticket'] = $parking->generateTicket();
        event(new ChangeParkingStatus($user, $parking, Ticket::PRINTED_STATUS));
      }
      return response()->json($return_arr);
    }

    public function getParkingCost(Parking $parking, $guid)
    {
      $ticket = $parking->findTicket($guid);
      return response()->json(['parking_cost' => $ticket->getParkingCost()]);
    }

    public function confirmParkingPayment(Parking $parking, Request $request)
    {
      $guid = $request->input('ticket_guid');
      $ticket = $parking->findTicket($guid);
      if( $request->input('ticket_status') == Ticket::PAID_STATUS && $ticket)
      {
        $ticket->confirmTicketPayment();
        return response()->json(['confirm_payment' => true]);
      }else{
        throw new InvalidInputException;
      }
    }

    public function canVehicleExit(Parking $parking, $guid)
    {
      $can_exit = $parking->canExit($guid);
      return response()->json(['Is_Exit_Permitted' => $can_exit]);
    }

    public function confirmVehicleExited(Parking $parking, Request $request)
    {
      $guid = $request->input('ticket_guid');
      $ticket = $parking->findTicket($guid);
      $ticket->confirmVehicleExited();

      $user = false;
      $email = $request->input('email');
      if($email){
        $user = User::getUserByEmail($email);
      }
      event(new ChangeParkingStatus($user, $parking, Ticket::EXIT_STATUS));

      return response()->json(['Is_Exit_Confirmed' => true]);
    }


    public function addUserToQueue(Parking $parking, Request $request)
    {
      $email = $request->input('email');
      $user = User::getUserByEmail($email);
      $is_added = $parking->addUserToQueue($user);
      return response()->json(['added_to_queue' => $is_added]);
    }

    // public function removeUserFromQueue(Parking $parking, Request $request)
    // {
    //   $email = $request->input('email');
    //   $user = User::getUserByEmail($email);
    //   $is_added = $parking->addUserToQueue($user);
    //   return response()->json(['added_to_queue' => $is_added]);
    // }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parking = Parking::create($request->all());
        return response()->json($parking, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Parking $parking)
    {
        return $parking;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Parking $parking)
    {
        $parking->update($request->all());
        return response()->json($parking, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parking $parking)
    {
        $parking->delete();
        return response()->json(null, 204);
    }
}
