<?php

namespace App\Listeners;

use App\Events\ChangeParkingStatus;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Parking;
use App\Ticket;
use App\Notifications\ParkingBecomeAvailable;
use App\Notifications\ParkingBecomeUnavailable;

class UpdateParkingQueue //implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ChangeParkingStatus  $event
     * @return void
     */
    public function handle(ChangeParkingStatus $event)
    {
        Log::info("Listener UpdateParkingQueue is triggered");
        Parking::updateParkingQueue($event);
    }











}
