<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ticket;
use App\User;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Log;
use App\Notifications\ParkingBecomeAvailable;
use App\Notifications\ParkingBecomeUnavailable;

class Parking extends Model
{
    public $timestamps = true;

    public function tickets()
    {
      return $this->hasMany('App\Ticket');
    }

    public function queued_users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function findTicket($guid)
    {
      return $this->tickets()->where('guid', $guid)->first();
    }

    public function spacesTaken()
    {
      return $this->tickets()->whereNull('exit_time')->count();
    }

    public function isParkingAvailable()
    {
      return ( $this->total_spaces > $this->spacesTaken() );
    }

    public function hasOneSpaceLeft()
    {
      $left = $this->total_spaces - $this->spacesTaken();
      return ( $left == 1 );
    }


    public function generateTicket()
    {
      //guid, parking name, timestamp,
      $ticket = new Ticket;
      $ticket->parking_id = $this->id;
      $ticket->guid = (string) Uuid::generate();
      $ticket->current_state = Ticket::PRINTED_STATUS;
      $ticket->save();
      return [
        'guid' => $ticket->guid,
        'generated_time' => (string) $ticket->created_at,
        'parking_lot_name' => $this->name
      ];
    }


    public function canExit($guid)
    {
      $count = $this->tickets()->where('guid', $guid)->where('current_state', Ticket::PAID_STATUS)->count();
      return ( $count == 1 ? true : false);
    }

    public function addUserToQueue(User $user)
    {
      $is_added = false;
      if($this->queued_users->contains($user) == false)
      {
        $this->queued_users()->save($user);
        $is_added = true;
      }
      return $is_added;
    }

    public function removeUserFromQueue(User $user)
    {
      $is_removed = false;
      if($this->queued_users->contains($user))
      {
        $this->queued_users()->detach($user->id);
        $is_removed = true;
      }
      return $is_removed;
    }

    public static function notifyUsers($func, $parking)
    {
      $queued_users = $parking->queued_users()->get();
      if($queued_users->isNotEmpty()){
        foreach ($queued_users as $user) {
            $user->notify($func($user, $parking));
        }
      }
    }


    public static function updateParkingQueue($event)
    {
      $func = false;
      if($event->status == Ticket::PRINTED_STATUS){
        if($event->user){
          $event->parking->removeUserFromQueue($event->user);
        }

        if( ! $event->parking->isParkingAvailable() ){
          $func = function($user, $parking){
            return new ParkingBecomeUnavailable($user, $parking);
          };
        }
      }elseif ($event->status == Ticket::EXIT_STATUS) {
        if($event->parking->hasOneSpaceLeft()){
          $func = function($user, $parking){
            return new ParkingBecomeUnavailable($user, $parking);
          };
        }
      }

      if($func){
        Parking::notifyUsers($func, $event->parking);
      }
    }




}
