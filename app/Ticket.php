<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use App\Exceptions\NoSpaceAvailableError;
use Carbon\Carbon;


class Ticket extends Model
{
    public $timestamps = true;

    protected $fillable = ['guid', 'exit_time'];

    protected $dates = [
        'created_at',
        'updated_at',
        'exit_time'
    ];

    const PAID_STATUS = 'paid';
    const PRINTED_STATUS = 'printed';
    const EXIT_STATUS = 'exited';

     //increase price in different tiers from biggest to smallest
    protected $starting_rate = 3;
    protected $increased_ratio = 1.5;



    public function parking()
    {
      return $this->belongsTo('App\Parking');
    }

    public function getParkingHours()
    {
      $time = Carbon::now();
      $minutes = $time->diffInMinutes($this->created_at);
      return ceil($minutes/60); //round up number of minutes to hours
    }

     public function getParkingCost()
     {
       $total_hours = $this->getParkingHours();
       $total_cost =  0;
       $days = floor($total_hours/24);
       $hours = $total_hours % 24;
       $daily_rate = $this->starting_rate * pow($this->increased_ratio,3);

       if($hours > 24){
         $total_cost = $daily_rate * floor($hours/24);
         $hours = $hours % 24;
       }

       if($hours > 6){
         $total_cost = $total_cost + $daily_rate;
       }elseif ($hours > 3) {
         $six_hours_rate = $this->starting_rate * pow($this->increased_ratio,2);
         $total_cost = $total_cost + $six_hours_rate;
       }elseif ($hours > 1) {
         $three_hours_rate = $this->starting_rate * pow($this->increased_ratio,1);
         $total_cost = $total_cost + $three_hours_rate;
       }else{
        $total_cost = $total_cost + $this->starting_rate;
       }

       return $total_cost;
     }

     public function confirmTicketPayment()
     {
       if($this->current_state != Ticket::PAID_STATUS){
         $this->current_state = Ticket::PAID_STATUS;
         $this->save();
       }
     }

     public function confirmVehicleExited()
     {
       if($this->current_state != Ticket::EXIT_STATUS){
         $this->exit_time = Carbon::now();
         $this->current_state = Ticket::EXIT_STATUS;
         $this->save();
       }
     }


}
