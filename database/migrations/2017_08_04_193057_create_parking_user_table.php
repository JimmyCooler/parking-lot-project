<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_user', function (Blueprint $table) {
           $table->integer('user_id')->unsigned()->nullable();
           $table->foreign('user_id')->references('id')
                 ->on('users')->onDelete('cascade');

           $table->integer('parking_id')->unsigned()->nullable();
           $table->foreign('parking_id')->references('id')
                 ->on('parkings')->onDelete('cascade');

           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_user');
    }
}
