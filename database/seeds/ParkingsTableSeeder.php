<?php

use Illuminate\Database\Seeder;
use App\Parking;

class ParkingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parking_name = 'banana_parking_lot';
        $total_spaces = 100;
        Parking::firstOrCreate(
            ['name' => $parking_name], ['total_spaces' => $total_spaces]
        );
        // if( App\Parking::where('name', $parking_name)->count() == 0){
        //   DB::table('parkings')->insert([
        //     'total_spaces' => $total_spaces,
        //     'name' => $parking_name,
        //  ]);
        // }
    }
}
