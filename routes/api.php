<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::post('login', 'API\UserController@login')->name('api.login');
Route::post('register', 'API\UserController@register')->name('api.register');

// Route::group(['middleware' => 'auth:api'], function(){
// 	Route::post('details', 'API\UserController@details');
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//http://localhost:5000/api/parkings/1
Route::resource('parkings', 'ParkingController', ['except' => ['create', 'edit']]);

//implicit model binding
Route::post('parkings/{parking}/tickets/generate_ticket', 'ParkingController@generateTicket')->name('parkings.generate_ticket');
Route::get('parkings/{parking}/tickets/parking_cost/{guid}', 'ParkingController@getParkingCost')->name('parkings.parking_cost');
Route::post('parkings/{parking}/tickets/confirm_payment', 'ParkingController@confirmParkingPayment')->name('parkings.confirm_payment');
Route::get('parkings/{parking}/tickets/check_exit_permission/{guid}', 'ParkingController@canVehicleExit')->name('parkings.check_exit_permission');
Route::post('parkings/{parking}/users/add_to_queue', 'ParkingController@addUserToQueue')->name('parkings.add_user');
Route::post('parkings/{parking}/tickets/confirm_exit', 'ParkingController@confirmVehicleExited')->name('parkings.confirm_exit');
//Route::post('parkings/{parking}/users/remove_to_queue', 'ParkingController@removeUserFromQueue')->name('parkings.remove_user');
