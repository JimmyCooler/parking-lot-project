<?php

namespace Tests\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Parking;
use App\Ticket;
use App\User;
use Tests\Helpers\Abstracts\AbstractParking;

//phpunit tests/Controllers/ParkingControllerTest
class ParkingControllerTest extends AbstractParking
{

   public function setup()
   {
     parent::setUp();
     $this->total_spaces = 50;
     $this->occupied_spaces = 30;
     $this->generateParkingAndTickets();
     $this->parking = Parking::first();
   }
   /*
    Testing http://localhost:5000/api/parkings/1
   */
    public function testGetResource()
    {
      $response = $this->call('GET', '/api/parkings');
      $this->assertEquals(200, $response->status());
    }

    /**
     * 1. If the parking parage is full, deny entry to that customer
     * 2. If there is space available, ticket info is printed.
    **/
    //phpunit --filter testGenerateTicketWithResult tests/Controllers/ParkingControllerTest
    public function testGenerateTicketWithResult()
    {
      //without user email
      $parking_id = $this->parking->id;
      $url = route('parkings.generate_ticket', ['parking' => $parking_id]);
      $response = $this->call('POST', $url);
      $response->assertStatus(200)->assertJsonStructure(['has_parking','ticket']);
      $this->assertTrue($response->original['has_parking']);

      //with valid user email
      $user = factory('App\User')->create();
      $user = User::getUserByEmail($user->email);
      $response = $this->call('POST', $url, ['email'=>$user->email]);
      $this->assertTrue($response->original['has_parking']);


      //After occupying all spaces, no ticket would be printed
      $this->occupyAllSpaces();
      $response = $this->call('POST', $url);
      $response->assertStatus(200)->assertJsonStructure(['has_parking']);
      $this->assertFalse($response->original['has_parking']);


      //$response = $this->call('POST', $url, ['email'=>'thisemail@example.com']);


    }

    public function testGenerateTicketWithoutResult()
    {
      $parking_id = $this->parking->id;
      $this->parking->delete();
      $url = route('parkings.generate_ticket', ['parking' => $parking_id]);
      $response = $this->call('POST', $url);
      $response->assertStatus(404)
               ->assertJson(["data" => "Resource not found"]);
    }

    public function testParkingCost()
    {
      $diff = 0.3; //30 minutes parking
      $this->parkingCostEvaluate($diff);
      $this->parkingCostEvaluate(1);
      $this->parkingCostEvaluate(3);
      $this->parkingCostEvaluate(6);
      $this->parkingCostEvaluate(20);
      $this->parkingCostEvaluate(45);
    }

    public function testConfirmPaymentViaWeb()
    {
      $parking_id = $this->parking->id;
      $ticket = $this->parking->tickets()->first();
      $url = route('parkings.confirm_payment', ['parking' => $parking_id]);
      $response = $this->json('POST', $url, ['ticket_guid' => $ticket->guid, 'ticket_status' => Ticket::PAID_STATUS]);
      $response->assertStatus(200)->assertJson(['confirm_payment' => true]);
      $ticket2 = $ticket->fresh();
      $this->assertEquals('paid', $ticket2->current_state);
    }

    public function testConfirmPaymentWithWrongInput()
    {
      $parking_id = $this->parking->id;
      $ticket = $this->parking->tickets()->first();
      $url = route('parkings.confirm_payment', ['parking' => $parking_id]);
      $response = $this->json('POST', $url, ['ticket_guid' => $ticket->guid, 'ticket_status' => 'banana']);
      $response->assertStatus(500)->assertJson(['error' => "Inputs are invalid"]);
    }

    public function testCanVehicleExit()
    {
      //Before parking payment is confirmed
      $parking_id = $this->parking->id;
      $ticket = $this->parking->tickets()->first();
      $url = route('parkings.check_exit_permission', ['parking' => $parking_id, 'guid' => $ticket->guid]);
      $response = $this->call('GET', $url);
      $response->assertStatus(200)->assertJson(['Is_Exit_Permitted' => false]);

      //After parking payment is confirmed
      $ticket->confirmTicketPayment();
      $response = $this->call('GET', $url);
      $response->assertStatus(200)->assertJson(['Is_Exit_Permitted' => true]);
    }


    public function testConfirmVehicleExited()
    {
      $parking_id = $this->parking->id;
      $ticket = $this->parking->tickets()->first();
      $url = route('parkings.confirm_exit', ['parking' => $parking_id]);
      $response = $this->json('POST', $url, ['ticket_guid' => $ticket->guid]);
      $response->assertStatus(200)->assertJson(['Is_Exit_Confirmed' => true]);
    }


    public function testAddUserToQueue()
    {
      $parking_id = $this->parking->id;
      $ticket = $this->parking->tickets()->first();

      $user = factory('App\User')->create();
      $user = User::getUserByEmail($user->email);
      $url = route('parkings.add_user', ['parking' => $parking_id]);
      $response = $this->json('POST', $url, ['email' => $user->email]);
      $response->assertStatus(200)->assertJson(['added_to_queue' => true]);

      $response = $this->json('POST', $url, ['email' => $user->email]);
      $response->assertStatus(200)->assertJson(['added_to_queue' => false]);
    }














}
