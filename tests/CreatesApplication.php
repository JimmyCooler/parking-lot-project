<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Dotenv\Dotenv;



trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        //putenv('DB_DEFAULT=sqlite_testing');
        //$dotenv = new Dotenv(__DIR__.'/../.env.testing');
        //$dotenv->overload();
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }


}
