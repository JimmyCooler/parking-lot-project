<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Notifications\OrderShipped;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Parking;
use App\User;
use App\Notifications\ParkingBecomeAvailable;
use App\Notifications\ParkingBecomeUnavailable;
//phpunit tests/Feature/ParkingNotificationTest.php

class ParkingNotificationTest extends TestCase
{
    public function testOrderShipping()
    {
        Notification::fake();

        $user = factory('App\User')->create();
        $user = User::getUserByEmail($user->email);
        $parking = factory('App\Parking')->create();
        $parking = Parking::where(['name'=>$parking->name])->first();

        //perform notification
        $user->notify(new ParkingBecomeAvailable($user, $parking));
        Notification::assertSentTo(
            [$user], ParkingBecomeAvailable::class
        );

        $user->notify(new ParkingBecomeUnavailable($user, $parking));
        Notification::assertSentTo(
            [$user], ParkingBecomeUnavailable::class
        );

    }
}
