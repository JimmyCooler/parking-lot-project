<?php

namespace Tests\Helpers\Abstracts;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Ticket;
use App\Parking;
use Carbon\Carbon;



abstract class AbstractParking extends TestCase
{

    protected $total_spaces, $occupied_spaces, $parking;


    public function generateParkingAndTickets()
    {
      factory('App\Parking', 1)->create(['total_spaces' => $this->total_spaces])->each(function ($parking) {
                                    for ($x = 0; $x < $this->occupied_spaces; $x++) {
                                        $parking->tickets()->save(factory('App\Ticket')->make());
                                     }
                                  });
    }

    public function occupyAllSpaces()
    {
      $remaining_spaces = $this->total_spaces - $this->occupied_spaces;
      for ($x = 0; $x < $remaining_spaces; $x++) {
          $this->parking->tickets()->save(factory('App\Ticket')->make());
       }
    }

    public function goBackInTime($ticket, $diff)
    {
      $now = Carbon::now();
      $diff = $diff * 60;
      $ticket->created_at = $now->copy()->subMinutes($diff);
      $ticket->save();
      return $ticket;
    }

    public function checkCostAtDiff($diff, $expected_cost, $ticket)
    {
      $diff = 0.5; //30 minutes parking
      $ticket = $this->goBackInTime($ticket, $diff);
      $expected_cost = 3;
      $actual_cost = $ticket->getParkingCost();
      $this->assertEquals($expected_cost, $actual_cost);
    }

    public function parkingCostEvaluate($diff)
    {
      // 1 hour or less parking
      $parking_id = $this->parking->id;
      $ticket = $this->parking->tickets()->first();
      $new_ticket = $this->goBackInTime($ticket, $diff);
      $url = route('parkings.parking_cost', ['parking' => $parking_id, 'guid' => $ticket->guid ]);
      $response = $this->call('GET', $url);
      $response->assertStatus(200)->assertJson(['parking_cost' => $new_ticket->getParkingCost()]);
    }
}
