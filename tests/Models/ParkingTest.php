<?php

namespace Tests\Models;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Notification;
use App\Ticket;
use App\Parking;
use App\User;
use Carbon\Carbon;
use App\Events\ChangeParkingStatus;
use App\Notifications\ParkingBecomeAvailable;
use App\Notifications\ParkingBecomeUnavailable;

use Tests\Helpers\Abstracts\AbstractParking;

//phpunit tests/Models/ParkingTest
class ParkingTest extends AbstractParking
{

     //use DatabaseMigrations;

     //run php artisan migrate --env=testing to start testing
     public function setup()
     {
       parent::setUp();
       //$this->parking = factory('App\Parking')->create();
       $this->total_spaces = 50;
       $this->occupied_spaces = 30;
       $this->generateParkingAndTickets();
       $this->parking = Parking::first();
     }

     public function testSpacesTaken()
     {
       $actual = $this->parking->spacesTaken();
       $this->assertEquals($this->occupied_spaces, $actual);
     }

     public function testIsParkingAvailable()
     {
       $actual = $this->parking->isParkingAvailable();
       $this->assertTrue($actual);

       //After occupied all spaces
       $this->occupyAllSpaces();
       $actual = $this->parking->isParkingAvailable();
       $this->assertFalse($actual);
     }

     public function testGenerateTicket()
     {
       $ticketInfo = $this->parking->generateTicket();
       $keys = ['guid', 'generated_time', 'parking_lot_name'];
       $this->assertEquals($keys, array_keys($ticketInfo));
       $this->assertNotNull($ticketInfo['guid']);
       $this->assertNotNull($ticketInfo['generated_time']);
       $this->assertEquals($this->parking->name, $ticketInfo['parking_lot_name']);
     }

     //phpunit --filter testFindTicket tests/Models/ParkingTest.php
     public function testFindTicket()
     {
        $ticket = $this->parking->tickets()->first();
        $new_ticket = $this->parking->findTicket($ticket->guid);
        $this->assertEquals($ticket->guid, $new_ticket->guid);
     }

     public function testGetParkingHours()
     {
        $ticket = $this->parking->tickets()->first();
        $diff = 10; //10 hours ago
        $ticket = $this->goBackInTime($ticket, $diff);
        $this->assertEquals($diff, $ticket->getParkingHours());
     }

     public function testGetParkingCost()
     {
       $ticket = $this->parking->tickets()->first();
       $this->checkCostAtDiff(0.5, 3, $ticket);
       $this->checkCostAtDiff(1.5, 3*1.5, $ticket);
       $this->checkCostAtDiff(3.5, 3*1.5*1.5, $ticket);

       $daily_rate = 3*1.5*1.5*1.5;
       $this->checkCostAtDiff(6, $daily_rate, $ticket);
       $this->checkCostAtDiff(10, $daily_rate, $ticket);

       $daily = 3*1.5*1.5*1.5;
       $expected_cost = $daily*floor(30/24) + $daily;
       $this->checkCostAtDiff(10, $daily_rate, $ticket);
     }

     public function testConfirmPayment()
     {
        $ticket = $this->parking->tickets()->first();
        $this->assertEquals(Ticket::PRINTED_STATUS, $ticket->current_state);
        $this->assertNull($ticket->exit_time);
        $ticket->confirmTicketPayment($ticket->guid);
        $ticket1 = $ticket->fresh();
        $this->assertEquals(Ticket::PAID_STATUS, $ticket1->current_state);
     }


     public function testCanExit()
     {
       //Before payment
       $ticket = $this->parking->tickets()->first();
       $ticket->current_state = Ticket::PRINTED_STATUS;
       $ticket->save();
       $actual = $this->parking->canExit($ticket->guid);
       $this->assertFalse($actual);

       //After payment
       $ticket->confirmTicketPayment($ticket->guid);
       $actual = $this->parking->canExit($ticket->guid);
       $this->assertTrue($actual);
     }

     public function testConfirmVehicleExited()
     {
       $ticket = $this->parking->tickets()->first();
       $this->assertNull($ticket->exit_time);
       $this->assertEquals(Ticket::PRINTED_STATUS, $ticket->current_state);
       $ticket->confirmVehicleExited();
       $ticket2 = $ticket->fresh();
       $this->assertNotNull($ticket2->exit_time);
       $this->assertEquals(Ticket::EXIT_STATUS, $ticket2->current_state);
     }

     public function testAddUserToQueue()
     {
       $user = factory('App\User')->create();
       $user = User::getUserByEmail($user->email);
       $actual = $this->parking->addUserToQueue($user);
       $this->assertTrue($actual);

       $user = $user->fresh();
       $this->parking = $this->parking->fresh();
       $actual = $this->parking->addUserToQueue($user);
       $this->assertFalse($actual);
     }

       //phpunit --filter testUpdateParkingQueue tests/Models/ParkingTest.php
     public function testUpdateParkingQueueWithFullGarage()
     {

       Notification::fake();
       $user1 = factory('App\User')->create();
       $user1 = User::getUserByEmail($user1->email);
       $user2 = factory('App\User')->create();
       $user2 = User::getUserByEmail($user2->email);
       $this->parking->addUserToQueue($user2);
       $this->occupyAllSpaces();
       $event = new ChangeParkingStatus($user1, $this->parking, Ticket::PRINTED_STATUS);
       Parking::updateParkingQueue($event);

       Notification::assertSentTo(
           [$user2], ParkingBecomeUnavailable::class
       );

     }

      //phpunit --filter testUpdateParkingQueueWithSpace tests/Models/ParkingTest.php
     public function testUpdateParkingQueueWithSpace()
     {

       Notification::fake();
       $user1 = factory('App\User')->create();
       $user1 = User::getUserByEmail($user1->email);
       $this->parking->addUserToQueue($user1);
       $this->occupyAllSpaces();

       $ticket = $this->parking->tickets()->first();
       $ticket->confirmVehicleExited();

       $event = new ChangeParkingStatus($user1, $this->parking, Ticket::EXIT_STATUS);
       Parking::updateParkingQueue($event);

       Notification::assertSentTo(
           [$user1], ParkingBecomeUnavailable::class
       );

     }





}
