<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;  //take care of all migration for each tests
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp(); // Don't forget this!
        //Artisan::call('migrate');
        //Artisan::call('db:seed');
    }


    public function tearDown()
    {
        //Artisan::call('migrate:reset');
        parent::tearDown();
    }
}
